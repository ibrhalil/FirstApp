//
//  ViewController.swift
//  FirstApp
//
//  Created by Halil Aydın on 27.09.2023.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func changeView(_ sender: Any) {
        imageView.image = UIImage(named: "Screenshot 2023-09-27 at 18.24.36");
    }
    

}

